import 'dart:convert';
import 'package:flutter_front/common/base_client.dart';
import 'package:flutter_front/constats.dart';
import 'package:flutter_front/contacts/models/contact.dart';

class ContactService extends BaseClient {
  final _jsonEnoder = JsonEncoder();

  Future<List<Contact>> getAllContacts() async {
    final response = await makeResponse(path: 'contacts', requestType: GET);
    return contactsFromJson(response.body);
  }

  Future<Contact> getContactById(int id) async {
    final response = await makeResponse(path: 'contacts', requestType: GET);
    return contactFromJson(response.body, id);
  }

  Future<void> addContact(Contact contact) async {
    await makeResponse(
      path: 'contacts',
      requestType: POST,
      body: jsonEncode(contact.toJson()),
    );
  }

  Future<void> editContact(int? id, Contact contact) async {
    Map<String, dynamic> data = {
      'first_name': contact.firstName,
      'last_name': contact.lastName,
      'email': contact.email,
      'phone': contact.phone,
    };

    final oneFieldIsNull = data.values.any((element) => element == null);

    if (oneFieldIsNull) {
      data.removeWhere((key, value) => value == null);
    }

    final response = await makeResponse(path: 'contacts/$id', requestType: oneFieldIsNull ? PATCH : PUT, body: _jsonEnoder.convert(data));
    print(response.statusCode);
  }

  Future<void> deleteContact(int id) async => await makeResponse(path: 'contacts/$id', requestType: DELETE);
}

part of 'contacts_bloc.dart';

@immutable
abstract class ContactsState {}

class ContactsInitial extends ContactsState {}

class ContactsLoading extends ContactsState {}

class ContactsSuccessLoaded extends ContactsState {
  final List<Contact> contacts;

  ContactsSuccessLoaded({required this.contacts});
}

class ContactsFailedLoaded extends ContactsState {
  final String error;

  ContactsFailedLoaded({required this.error});
}

class ContactSuccessAdd extends ContactsState {}

class ContactFailedAdd extends ContactsState {
  final String error;

  ContactFailedAdd({required this.error});
}

class ContactSuccessEdit extends ContactsState {}

class ContactFailedEdit extends ContactsState {
  final String error;

  ContactFailedEdit({required this.error});
}

class ContactSuccessDelete extends ContactsState {}

class ContactFailedDelete extends ContactsState {
  final String error;

  ContactFailedDelete({required this.error});
}

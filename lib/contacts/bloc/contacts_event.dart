part of 'contacts_bloc.dart';

@immutable
abstract class ContactsEvent {}

class ContactsRequest extends ContactsEvent {
  ContactsRequest(this.status);

  final status;
}

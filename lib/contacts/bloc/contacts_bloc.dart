// ignore_for_file: invalid_use_of_visible_for_testing_member

import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_front/contacts/models/contact.dart';
import 'package:flutter_front/contacts/service/contact_service.dart';
import 'package:meta/meta.dart';

part 'contacts_event.dart';
part 'contacts_state.dart';

class ContactsBloc extends Bloc<ContactsEvent, ContactsState> {
  final ContactService _contactService = ContactService();

  ContactsBloc() : super(ContactsInitial()) {
    on<ContactsEvent>((event, emit) {});
  }

  Future<void> loadContacts() async {
    emit(ContactsInitial());

    try {
      emit(ContactsLoading());

      final contacts = await Future.delayed(Duration(seconds: 1), () async {
        return await _contactService.getAllContacts();
      });

      // final contacts = await _contactService.getAllContacts();

      emit(ContactsSuccessLoaded(contacts: contacts));
    } catch (exp) {
      print(exp);
      emit(ContactsFailedLoaded(error: exp.toString()));
    }
  }

  Future<void> addContact(Contact contact) async {
    try {
      await _contactService.addContact(contact);
      emit(ContactSuccessAdd());
      loadContacts();
    } catch (exp) {
      print(exp);
      emit(ContactFailedAdd(error: exp.toString()));
    }
  }

  Future<void> editContact(int contactId, Contact contact) async {
    try {
      await _contactService.editContact(contactId, contact);
      emit(ContactSuccessEdit());
      loadContacts();
    } catch (exp) {
      print(exp);
      emit(ContactFailedEdit(error: exp.toString()));
    }
  }

  Future<void> deleteContact(int id) async {
    try {
      await _contactService.deleteContact(id);
      emit(ContactSuccessDelete());
      loadContacts();
    } catch (exp) {
      print(exp);
      emit(ContactFailedDelete(error: exp.toString()));
    }
  }
}

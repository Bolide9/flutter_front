import 'dart:convert';

List<Contact> contactsFromJson(String str) => List<Contact>.from(json.decode(str).map((x) => Contact.fromJson(x)));

Contact contactFromJson(String str, int id) => List<Contact>.from(json.decode(str).map((x) => Contact.fromJson(x))).firstWhere((el) => el.id == id);

String contactToJson(Contact data) => json.encode(data.toJson());

String contactsToJson(List<Contact> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Contact {
  Contact({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.phone,
  });

  int? id;
  String? email;
  String? firstName;
  String? lastName;
  String? phone;

  factory Contact.fromJson(Map<String, dynamic> json) => Contact(
        id: json["id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "phone": phone,
      };

  String getFullName() => '$firstName $lastName';
}

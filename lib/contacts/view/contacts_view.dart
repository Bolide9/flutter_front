import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_front/contacts/bloc/contacts_bloc.dart';
import 'package:flutter_front/contacts/view/contact_card.dart';
import 'package:flutter_front/widgets/refresher.dart';

import 'contact_view.dart';

class ContactsView extends StatelessWidget {
  Future<void> _onRefresh(BuildContext context) async => await context.read<ContactsBloc>().loadContacts();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => ContactView())),
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 24.0,
        ),
        backgroundColor: Colors.blue.shade900,
      ),
      body: SafeArea(
        minimum: EdgeInsets.only(top: 40),
        child: BlocBuilder<ContactsBloc, ContactsState>(
          builder: (context, state) {
            switch (state.runtimeType) {
              case ContactsLoading:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ContactsSuccessLoaded:
                state as ContactsSuccessLoaded;

                return Refresher(
                  onRefresh: () => _onRefresh(context),
                  child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    itemCount: state.contacts.length,
                    itemBuilder: (context, index) => ContactCard(
                      contact: state.contacts[index],
                    ),
                  ),
                );
              case ContactsFailedLoaded:
                state as ContactsFailedLoaded;

                return Refresher(
                  onRefresh: () => _onRefresh(context),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Text('Failed load data: ${state.error}'),
                    ),
                  ),
                );
              default:
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Text('Что-то пошло не так ${state}: ${state.runtimeType}'),
                  ),
                );
            }

            // if (state is ContactsLoading) {
            //   return Center(
            //     child: CircularProgressIndicator(),
            //   );
            // }

            // if (state is ContactsSuccessLoaded) {
            //   return Refresher(
            //     onRefresh: () => _onRefresh(context),
            //     child: GridView.builder(
            //       physics: const ClampingScrollPhysics(),
            //       gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            //       itemCount: state.contacts.length,
            //       itemBuilder: (context, index) {
            //         final contact = state.contacts[index];

            //         return Container(
            //           child: Card(
            //             child: InkWell(
            //               onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => ContactView(contact: contact))),
            //               child: ListTile(
            //                 title: Text(contact.firstName),
            //                 subtitle: Text(contact.lastName),
            //                 trailing: IconButton(
            //                   icon: Icon(Icons.delete, color: Colors.red),
            //                   splashRadius: 25,
            //                   onPressed: () => context.read<ContactsBloc>().deleteContact(contact.id),
            //                 ),
            //               ),
            //             ),
            //           ),
            //         );
            //       },
            //     ),
            //   );
            // }

            // if (state is ContactsFailedLoaded) {
            //   return Center(
            //     child: Padding(
            //       padding: const EdgeInsets.symmetric(horizontal: 10.0),
            //       child: Text('failed load data ${state.error}'),
            //     ),
            //   );
            // }

            // return Center(
            //   child: Padding(
            //     padding: const EdgeInsets.symmetric(horizontal: 10.0),
            //     child: Text('Что-то пошло не так ${state}: ${state.runtimeType}'),
            //   ),
            // );
          },
        ),
      ),
    );
  }
}

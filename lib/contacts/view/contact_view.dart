import 'package:flutter/material.dart';
import 'package:flutter_front/contacts/bloc/contacts_bloc.dart';
import 'package:flutter_front/contacts/models/contact.dart';
import 'package:provider/src/provider.dart';

class ContactView extends StatefulWidget {
  ContactView({Key? key, this.contact}) : super(key: key);

  final Contact? contact;

  @override
  State<ContactView> createState() => _ContactViewState();
}

class _ContactViewState extends State<ContactView> {
  final _focusName = FocusNode();
  final _focusLastName = FocusNode();
  final _focusEmail = FocusNode();
  final _focusPhone = FocusNode();

  final _nameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();

  void _requestFocus(dynamic _focusNode) {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
          splashRadius: 25,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 24),
            if (widget.contact?.id != null)
              Center(
                child: Text('ID ${widget.contact?.id.toString()}'),
              ),
            Padding(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                controller: _nameController,
                focusNode: _focusName,
                onTap: () => _requestFocus(_focusName),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  labelStyle: TextStyle(color: _focusName.hasFocus ? Colors.white : Colors.grey[800]),
                  hintStyle: TextStyle(color: _focusName.hasFocus ? Colors.white : Colors.white70),
                  hintText: widget.contact?.firstName ?? 'Имя',
                  fillColor: Colors.white70,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                controller: _lastNameController,
                focusNode: _focusLastName,
                onTap: () => _requestFocus(_focusLastName),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  // labelStyle: TextStyle(color: _focusLastName.hasFocus ? Colors.white : Colors.grey[800]),
                  hintStyle: TextStyle(color: _focusLastName.hasFocus ? Colors.white : Colors.grey),
                  hintText: widget.contact?.lastName ?? 'Фамилия',
                  fillColor: Colors.white70,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                controller: _emailController,
                focusNode: _focusEmail,
                onTap: () => _requestFocus(_focusEmail),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  labelStyle: TextStyle(color: _focusEmail.hasFocus ? Colors.white : Colors.grey[800]),
                  hintStyle: TextStyle(color: _focusEmail.hasFocus ? Colors.white : Colors.white70),
                  hintText: widget.contact?.email ?? 'Email',
                  fillColor: Colors.white70,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                controller: _phoneController,
                focusNode: _focusPhone,
                onTap: () => _requestFocus(_focusPhone),
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  labelStyle: TextStyle(color: _focusPhone.hasFocus ? Colors.white : Colors.white70),
                  floatingLabelStyle: TextStyle(color: _focusPhone.hasFocus ? Colors.white : Colors.white70),
                  hintStyle: TextStyle(color: _focusPhone.hasFocus ? Colors.white : Colors.white70),
                  hintText: widget.contact?.phone ?? 'Phone',
                  fillColor: Colors.white70,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(30),
              child: Container(
                width: 200,
                height: 30,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.red, Colors.blue],
                    begin: FractionalOffset(0.0, 0.0),
                    end: FractionalOffset(0.5, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp,
                  ),
                ),
                // ignore: deprecated_member_use
                child: RaisedButton(
                  color: Colors.transparent,
                  child: Text('Submit'),
                  onPressed: () {
                    final _contact = Contact();

                    _contact.firstName = _nameController.text.isNotEmpty ? _nameController.text : null;
                    _contact.lastName = _lastNameController.text.isNotEmpty ? _lastNameController.text : null;
                    _contact.email = _emailController.text.isNotEmpty ? _emailController.text : null;
                    _contact.phone = _phoneController.text.isNotEmpty ? _phoneController.text : null;

                    if (widget.contact != null && widget.contact?.id != null) {
                      context.read<ContactsBloc>().editContact(widget.contact!.id!, _contact);
                    } else {
                      context.read<ContactsBloc>().addContact(_contact);
                    }

                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

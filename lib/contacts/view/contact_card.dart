import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_front/contacts/bloc/contacts_bloc.dart';
import 'package:flutter_front/contacts/models/contact.dart';
import 'package:flutter_front/contacts/view/contact_view.dart';
import 'package:provider/src/provider.dart';

class ContactCard extends StatelessWidget {
  const ContactCard({Key? key, required this.contact}) : super(key: key);

  final Contact contact;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 30),
      child: InkWell(
        customBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            bottomLeft: Radius.circular(16),
          ),
        ),
        onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => ContactView(contact: contact))),
        splashColor: Colors.white12,
        child: ListTile(
          title: Text(
            contact.getFullName(),
          ),
          trailing: IconButton(
            icon: Icon(Icons.remove, color: Colors.red, size: 24),
            splashRadius: 25,
            onPressed: () => context.read<ContactsBloc>().deleteContact(contact.id!),
          ),
        ),
      ),
    );
  }
}


//  Card(
//         child: InkWell(
//           onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => ContactView(contact: contact))),
//           child: ListTile(
//             title: Text(contact.firstName),
//             subtitle: Text(contact.lastName),
//             trailing: IconButton(
//               icon: Icon(Icons.delete, color: Colors.red),
//               splashRadius: 25,
//               onPressed: () => context.read<ContactsBloc>().deleteContact(contact.id),
//             ),
//           ),
//         ),
//       ),
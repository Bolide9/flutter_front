import 'package:flutter_front/config.dart';
import 'package:flutter_front/constats.dart';
import 'package:http/http.dart' as http;

class BaseClient {
  Future<http.Response> makeResponse<T>({required String path, required String requestType, T? body}) async {
    final url = '$host/$path';
    final uri = Uri.parse(url);

    switch (requestType) {
      case POST:
        return await http.post(uri, headers: {'Content-Type': 'application/json'}, body: body);
      case GET:
        return await http.get(uri);
      case PUT:
        return await http.put(uri, headers: {'Content-Type': 'application/json'}, body: body);
      case PATCH:
        return await http.patch(uri, headers: {'Content-Type': 'application/json'}, body: body);
      case DELETE:
        return await http.delete(uri);
      default:
        throw Exception('Not found request type');
    }
  }
}

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_front/constats.dart';

enum platforms {
  Android,
  Web,
  Unknown,
}

String get webHost => HOST;
String get mobileHost => MOBILE_HOST;
dynamic get device => !kIsWeb ? platforms.Android : platforms.Web;
String get host => hosts[device];

final Map hosts = {
  platforms.Android: mobileHost,
  platforms.Web: webHost,
};
